package com.plg.springbootsimple.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.plg.springbootsimple.annotation.AnnotationTest;

@Service
public class AnnoService {
    private static Logger logger = LoggerFactory.getLogger(AnnoService.class);


    @AnnotationTest(log = "methodA", timeout = 22)
    public String methodA(String msg) {
        logger.info("methodA msg ===== " + msg);

        return msg;
    }


    @AnnotationTest(log = "methodB", timeout = 100)
    public void methodB() {
        logger.info("methodB ====== ");
    }

}
