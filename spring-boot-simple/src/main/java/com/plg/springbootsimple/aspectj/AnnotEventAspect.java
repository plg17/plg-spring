package com.plg.springbootsimple.aspectj;

import java.lang.reflect.Method;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import com.plg.springbootsimple.annotation.AnnotationTest;
import com.plg.springbootsimple.service.AnnoService;

@Service
public class AnnotEventAspect implements ApplicationListener<ContextRefreshedEvent> {
    private static Logger logger = LoggerFactory.getLogger(AnnotEventAspect.class);

    @Resource
    private Environment env;

    @Resource
    private AnnoService annoService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        logger.info("onApplicationEvent spring event =========");
        Method[] methods = annoService.getClass().getDeclaredMethods();

        for (Method method : methods) {
            //
            AnnotationTest annot = method.getAnnotation(AnnotationTest.class);

            logger.info("onApplicationEvent log == " + annot.log());
            logger.info("onApplicationEvent timeout == " + annot.timeout());
        }


    }

}
