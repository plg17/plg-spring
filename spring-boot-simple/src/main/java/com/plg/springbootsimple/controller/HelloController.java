package com.plg.springbootsimple.controller;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.plg.springbootsimple.service.AnnoService;

@Controller
public class HelloController {
    private static Logger logger = LoggerFactory.getLogger(HelloController.class);

    @Resource
    private AnnoService annoService;

    @RequestMapping("/")
    public String index() {
        logger.info("首页---------");

        annoService.methodA("首页");
        annoService.methodB();

        return "/templates/index";
    }

    @RequestMapping("/hello")
    public String hello() {
        logger.info("hello================");
        return "/hello";
    }
}
