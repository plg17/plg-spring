package com.plg.springbootsimple.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface AnnotationTest {

    /**
     * 日志注解
     *
     * @return
     */
    String log() default "";

    /**
     * 超时时间，默认10s
     */
    int timeout() default 10;

}
