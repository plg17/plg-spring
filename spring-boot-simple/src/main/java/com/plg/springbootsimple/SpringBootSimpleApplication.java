package com.plg.springbootsimple;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * @SpringBootApplication：开启了Spring的组件扫描和Spring Boot的自动配置功能
 */
@SpringBootApplication
public class SpringBootSimpleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootSimpleApplication.class, args);
    }
}
