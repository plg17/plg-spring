package com.plg.springmvc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.plg.springmvc.model.Spitter;
import com.plg.springmvc.model.Spittle;

@Service
public class SpitterService implements ISpitterService {
    private static final Logger logger = LoggerFactory.getLogger(SpitterService.class);

    @Override
    public List<Spittle> getRecentSpittles(int count) {
        logger.info("SpitterService.getRecentSpittles, count:" + count);
        List<Spittle> recentSpittles = new ArrayList<Spittle>();
        Spittle spittle = new Spittle();
        spittle.setId(1L);

        Spitter spitter = new Spitter();
        spitter.setId(1000L);
        spitter.setFullName("panliguan");
        spitter.setPassword("243242");
        spitter.setUsername("plg17");
        spitter.setEmail("plg17@sina.com");
        spitter.setImage("/static/images/psu.jpg");

        List<Spittle> spittles = new ArrayList<Spittle>();
        spitter.setSpittles(spittles);
        spittle.setSpitter(spitter);

        spittle.setText("java");
        spittle.setWhen(new Date());
        recentSpittles.add(spittle);
        ///////////////////////////////////////////////////
        Spittle spittle1 = new Spittle();
        spittle1.setId(2L);

        Spitter spitter1 = new Spitter();
        spitter1.setId(1001L);
        spitter1.setFullName("panliguan1");
        spitter1.setPassword("2432421");
        spitter1.setUsername("plg17111");
        spitter1.setEmail("plg1711@sina.com");
        spitter.setImage("/static/images/20170802115613.png");

        List<Spittle> spittles1 = new ArrayList<Spittle>();
        spitter1.setSpittles(spittles1);
        spittle1.setSpitter(spitter1);
        spittle1.setText("java");
        spittle1.setWhen(new Date());
        recentSpittles.add(spittle1);

        ///////////////////////////////////////////////////
        Spittle spittle2 = new Spittle();
        spittle1.setId(3L);

        Spitter spitter2 = new Spitter();
        spitter2.setId(1002L);
        spitter2.setFullName("panliguan2");
        spitter2.setPassword("2432422");
        spitter2.setUsername("plg17112");
        spitter2.setEmail("plg17112@sina.com");
        spitter2.setImage("/static/images/20170802115630.png");

        List<Spittle> spittles2 = new ArrayList<Spittle>();
        spitter2.setSpittles(spittles2);
        spittle2.setSpitter(spitter2);
        spittle2.setText("java");
        spittle2.setWhen(new Date());
        recentSpittles.add(spittle2);

        ///////////////////////////////////////////////////
        Spittle spittle3 = new Spittle();
        spittle1.setId(4L);

        Spitter spitter3 = new Spitter();
        spitter3.setId(1003L);
        spitter3.setFullName("panliguan3");
        spitter3.setPassword("2432423");
        spitter3.setUsername("plg17113");
        spitter3.setEmail("plg17113@sina.com");
        spitter3.setImage("/static/images/20170802120343.png");

        List<Spittle> spittles3 = new ArrayList<Spittle>();
        spitter3.setSpittles(spittles3);
        spittle3.setSpitter(spitter3);
        spittle3.setText("java");
        spittle3.setWhen(new Date());
        recentSpittles.add(spittle3);

        ///////////////////////////////////////////////////
        Spittle spittle4 = new Spittle();
        spittle1.setId(5L);

        Spitter spitter4 = new Spitter();
        spitter4.setId(1004L);
        spitter4.setFullName("panliguan4");
        spitter4.setPassword("2432423");
        spitter4.setUsername("plg17114");
        spitter4.setEmail("plg17114@sina.com");
        spitter4.setImage("/static/images/psu.jpg");

        List<Spittle> spittles4 = new ArrayList<Spittle>();
        spitter4.setSpittles(spittles4);
        spittle4.setSpitter(spitter4);
        spittle4.setText("java");
        spittle4.setWhen(new Date());
        recentSpittles.add(spittle4);

        return recentSpittles;
    }


    @Override
    public void saveSpittle(Spittle spittle) {
        // TODO Auto-generated method stub

    }

    @Override
    public void saveSpitter(Spitter spitter) {
        // TODO Auto-generated method stub

    }

    @Override
    public Spitter getSpitter(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void startFollowing(Spitter follower, Spitter followee) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Spittle> getSpittlesForSpitter(Spitter spitter) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<Spittle> getSpittlesForSpitter(String username) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Spitter getSpitter(String username) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Spittle getSpittleById(long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void deleteSpittle(long id) {
        // TODO Auto-generated method stub

    }

    @Override
    public List<Spitter> getAllSpitters() {
        // TODO Auto-generated method stub
        return null;
    }

}
