package com.plg.springmvc.controller;

import java.util.Map;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.plg.springmvc.service.ISpitterService;

/*
 * @Controller注解表明这个类是一个控制器类。这个类是@Component注解的一种具体化，也就是说<context:component-scan>将查找使用@Component注解的类并将其注解为Bean
 */
@Controller
public class HomeController {
    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    private static final int DEFAULT_SPITTLES_PRE_PAGE = 25;

    @Resource
    private ISpitterService spitterService;

    @RequestMapping({"/", "/home"})
    public String showHomePage(Map<String, Object> model) {
        logger.info("HomeController.showHomePage");

        model.put("spittles", spitterService.getRecentSpittles(DEFAULT_SPITTLES_PRE_PAGE));

        // 返回逻辑视图名字
        return "home";
    }

    @RequestMapping({"/", "/login"})
    public String login(Model model, String j_username, String j_password) {
        logger.info("login");
        model.addAttribute("authUrl", "home");
        model.addAttribute("spittles", spitterService.getRecentSpittles(DEFAULT_SPITTLES_PRE_PAGE));

        // 返回逻辑视图名字
        return "login";
    }
}
