package com.plg.springmvc.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;

/**
 * AOP的通知类
 */
@Aspect
public class MyAspect {

    /**
     * 前置通知
     */
    @Before("execution(* com.plg.springmvc.service.ISpitterService.getRecentSpittles(..))")
    public void before() {
        System.out.println("前置通知....");
    }


    /**
     * 后置通知
     *
     * @param returnVal 切入方法执行后的返回值
     */
    // @AfterReturning(value = "execution (* com.plg.springmvc.service.ISpitterService.getRecentSpittles(..))", returning = "returnVal")
    public void AfterReturning(Object returnVal) {
        System.out.println("后置通知……" + returnVal);
    }


    /**
     * 环绕通知
     *
     * @param joinPoint
     * @return
     * @throws Throwable
     */
    // @Around("execution(* com.plg.springmvc.service.ISpitterService.getRecentSpittles(..))")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        System.out.println("环绕通知前....");
        Object object = (Object) joinPoint.proceed();
        System.out.println("环绕通知后....");
        return object;
    }

    /**
     * 跑出异常通知
     *
     * @param e
     */
    // @AfterThrowing(value = "execution(* com.plg.springmvc.service.ISpitterService.getRecentSpittles(..))", throwing = "e")
    public void afterThrowable(Throwable e) {
        System.out.println("出现异常：" + e.getMessage());
    }

    /**
     * 无论什么情况下都会执行的方法
     */
    // @After(value = "execution(* com.plg.springmvc.service.ISpitterService.getRecentSpittles(..))")
    public void after() {
        System.out.println("最终通知。。。");
    }


}
